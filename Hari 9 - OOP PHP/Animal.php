<?php

class Animal
{
    public $name;
    public $legs;
    public $cold_blooded;

    public function __construct()
    { }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setlegs($legs)
    {
        $this->legs = $legs;
    }

    public function setColdBlooded($cold_blooded)
    {
        $this->cold_blooded = $cold_blooded;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function getColdBlooded()
    {
        return $this->cold_blooded;
    }
}
