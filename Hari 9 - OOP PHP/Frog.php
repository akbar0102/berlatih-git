<?php

class Frog extends Animal
{
    public function __construct()
    { }

    public function jump()
    {
        return "Hop Hop";
    }
}
