<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new animal();
$sheep->setName("shaun");
$sheep->setlegs("4");
$sheep->setColdBlooded("no");

echo "Name: " . $sheep->getName() . "<br>";
echo "legs: " . $sheep->getLegs() . "<br>";
echo "cold blooded: " . $sheep->getColdBlooded() . "<br>";
echo "<br>";

$frog = new Frog();
$frog->setName("buduk");
$frog->setlegs("4");
$frog->setColdBlooded("no");

echo "Name: " . $frog->getName() . "<br>";
echo "legs: " . $frog->getLegs() . "<br>";
echo "cold blooded: " . $frog->getColdBlooded() . "<br>";
echo "Jump: " . $frog->jump() . "<br>";
echo "<br>";

$ape = new Ape();
$ape->setName("kera sakti");
$ape->setlegs("2");
$ape->setColdBlooded("no");

echo "Name: " . $ape->getName() . "<br>";
echo "legs: " . $ape->getLegs() . "<br>";
echo "cold blooded: " . $ape->getColdBlooded() . "<br>";
echo "Yell: " . $ape->yell() . "<br>";
